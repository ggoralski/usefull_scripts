#!/bin/bash

# Install some usefull bioinformatics, programming (Java) and other stuff

cd ~

sudo apt-get update
sudo apt-get upgrade

mkdir ~/DOWNLOADS
DOWNLOADS=~/DOWNLOADS
# 

sudo apt-get install git vim vim-gtk3 tmux mc curl zsh synaptic

# Oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# ImageJ

cd ~
wget http://wsr.imagej.net/distros/linux/ij152-linux64-java8.zip
unzip ij152-linux64-java8.zip
cd ImageJ
./ImageJ
cp ImageJ.desktop ../Desktop


# Samtools

#sudo apt-get install samtools

cd ~
# Do this uder BASH! (not zsh)
sudo apt install ncurses-* zlib1g-dev libbz2-dev liblzma-dev libcurl4-openssl-dev
cd ~/DOWNLOADS
wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2
bzip2 -d samtools-1.9.tar.bz2
tar -xvf samtools-1.9.tar
cd samtools-1.9
./configure
make
sudo make install
cd ..
rm -rf samtools-1.9

# mafft 

cd ~/DOWNLOADS
wget https://mafft.cbrc.jp/alignment/software/mafft-7.407-with-extensions-src.tgz
tar -xvzf mafft-7.407-with-extensions-src.tgz
cd mafft-7.407-with-extensions/core
make clean
make
sudo make install

# iqtree
cd ~/DOWNLOADS
wget https://github.com/Cibiv/IQ-TREE/releases/download/v1.6.9/iqtree-1.6.9-Linux.tar.gz
tar -xvzf iqtree*
sudo cp iqtree-1.6.9-Linux/bin/iqtree /usr/bin

## Dendropy 
sudo apt-get install python-pip
sudo pip install -U setuptools
sudo pip install -U dendropy

#PGDSpider

cd ~/DOWNLOADS
wget http://www.cmpg.unibe.ch/software/PGDSpider/PGDSpider_2.1.1.5.zip
unzip PGDSpider_*
mv PGDSpider_2.1.1.5 PGDSpider

# Some bioinfromatic stuff
sudo apt-get install catdoc muscle mrbayes clustalw clustalo clustalx t-coffee probcons biosquid hmmer phyml fasttree ncbi-blast+

# A lot of bioinformatics stuff
#sudo apt-get install med-bio 


# Java 8
sudo echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/webupd8team-java.list
sudo echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee -a /etc/apt/sources.list.d/webupd8team-java.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
sudo apt-get update
sudo apt-get install oracle-java8-installer
sudo update-alternatives --config java
sudo apt install oracle-java8-set-default


cd ~/DOWNLOADS

# Jalview
wget http://www.jalview.org/Web_Installers/InstData/Linux/NoVM/install-jalview.bin
chmod u+x install-jalview.bin
./install-jalview.bin

# Dendroscope
wget http://ab.inf.uni-tuebingen.de/data/software/dendroscope3/download/Dendroscope_unix_3_5_9.sh
chmod u+x Dendroscope_unix_3_5_9.sh
./Dendroscope_unix_3_5_9.sh

# AliView
wget http://ormbunkar.se/aliview/downloads/linux/linux-version-1.25/aliview.install.run
chmod +x aliview.install.run 
sudo ./aliview.install.run 

# FigTree
wget 'http://tree.bio.ed.ac.uk/download.php?id=96&num=3' -O FT.tgz
tar -xvzf FT.tgz
ft=`ls -d FigTree*`
cp $ft/bin/figtree $ft
chmod u+x $ft/figtree
cp -r $ft ~


# JmodelTest 2
cd ~
wget https://github.com/ddarriba/jmodeltest2/files/157117/jmodeltest-2.1.10.tar.gz
tar -xvzf jmodeltest-2.1.10.tar.gz
# zmiana nazwy katalogu na bardziej przyjazny i uniwersalny
mv jmodeltest-2.1.10 jmodeltest
# pobranie manuala do katalogu programu
cd jmodeltest
wget https://github.com/ddarriba/jmodeltest2/files/157130/manual.pdf

# Install bioawk
# https://silico-sciences.com/2015/12/13/install-bioawk-on-ubuntu/
sudo apt-get install build-essential
sudo apt-get install byacc
git clone git://github.com/lh3/bioawk.git
cd bioawk
make 
sudo cp bioawk /usr/local/bin
cd .. && rm -rf bioawk

# PRANK
mkdir ~/programs
cd ~/programs
wget http://wasabiapp.org/download/prank/prank.linux64.170427.tgz
tar -xvzf prank.linux64.170427.tgz
./prank/bin/prank
mkdir ~/bin
cp -R /home/$USER/programs/prank/bin/* ~/bin/



# Powerline fonts
cd ~
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts



# Netbeans

cd DOWNLOADS
wget http://bits.netbeans.org/netbeans/8.2/community/bundles/netbeans-8.2-javase-linux.sh
chmod u+x netbeans-8.2-javase-linux.sh
./netbeans-8.2-javase-linux.sh --jdkhome /usr/lib/jvm/java-8-oracle
~/netbeans-8.2/bin/netbeans --jdkhome /usr/lib/jvm/java-8-oracle

# Entrez Direct

cd ~
/bin/bash
perl -MNet::FTP -e \
    '$ftp = new Net::FTP("ftp.ncbi.nlm.nih.gov", Passive => 1);
    $ftp->login; $ftp->binary;
    $ftp->get("/entrez/entrezdirect/edirect.tar.gz");'
gunzip -c edirect.tar.gz | tar xf -
rm edirect.tar.gz
builtin exit
export PATH=$PATH:$HOME/edirect >& /dev/null || setenv PATH "${PATH}:$HOME/edirect"
./edirect/setup.sh
 echo "export PATH=\${PATH}:/home/student/edirect" >> $HOME/.bashrc
 
# Newick utils
#bash
cd ~/DOWNLOADS
#sudo apt install sudo apt install libluajit-5*
wget http://cegg.unige.ch/pub/newick-utils-1.6-Linux-x86_64-disabled-extra.tar.gz
tar -xvzf newick*
cd newick-utils-1.6
sudo cp src/nw_* /usr/local/bin


# JmodelTest 2

wget https://github.com/ddarriba/jmodeltest2/files/157117/jmodeltest-2.1.10.tar.gz
tar -xvzf jmodeltest-2.1.10.tar.gz
rm jmodeltest-2.1.10.tar.gz
mv jmodeltest* ~/

# Beagle
cd ~/DOWNLOADS
git clone --depth=1 https://github.com/beagle-dev/beagle-lib.git
cd beagle-lib
./autogen.sh
./configure --prefix=$HOME
make install
echo "export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH" >> ~/.bashrc

# BEAST - wersja 1.x
cd ~
wget https://github.com/beast-dev/beast-mcmc/releases/download/v1.8.4/BEASTv1.8.4.tgz
tar -xvzf BEASTv1.8.4.tgz
rm BEASTv1.8.4.tgz
mv BEASTv1.8.4.tgz BEAST

# r8s

sudo apt-get install make gcc gfortran 
cd ~
wget https://sourceforge.net/projects/r8s/files/r8s1.81.tar.gz
tar -xvzf r8s1.81.tar.gz
mv r8s1.81 r8s
cd r8s/src/   
sed -i 's/continuousML.o:continuousML.h/#continuousML.o:continuousML.h/' Makefile.linux
sed -i 's/blas.o tn.o TNwrapper.o continuousML.o ancestral.o covarion.o/blas.o tn.o TNwrapper.o ancestral.o covarion.o #continuousML.o/' Makefile.linux
sed -i 's|CFLAGS|LPATH = -L/usr/local/gfortran/lib # correct location as of August 2011.\nCFLAGS|' Makefile.linux
sed -i 's|memory.o: /usr/include/errno.h /usr/include/sys/errno.h|memory.o: /usr/include/errno.h # /usr/include/sys/errno.h|' Makefile.linux
sed -i 's|memory.o: /usr/include/stdlib.h memory.h|memory.o: /usr/include/stdlib.h # memory.h|' Makefile.linux
sed -i 's|CFLAGS = -g -std=c99 -pedantic|#CFLAGS = -g -std=c99 -pedantic |' Makefile.linux
make -f Makefile.linux
chmod u+x r8s
cp r8s ..
cd ..

# R 3.5 for Debian 9: https://linuxize.com/post/how-to-install-r-on-debian-9/
sudo apt install dirmngr apt-transport-https ca-certificates software-properties-common gnupg2
sudo apt-key adv --keyserver keys.gnupg.net --recv-key 'E19F5F87128899B192B1A2C2AD5F960A256A04AF'
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/debian stretch-cran35/'
sudo apt update
sudo apt install r-base build-essential
R --version

# Dropbox
cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
~/.dropbox-dist/dropboxd

# FAST - set of usefull tools for working with sequences
# see: https://github.com/tlawrence3/FAST

sudo perl -MCPAN -e 'install FAST'

#Spades

cd ~/DOWNLOADS
wget http://cab.spbu.ru/files/release3.11.1/SPAdes-3.11.1.tar.gz
tar -xzf SPAdes-3.11.1.tar.gz
cd SPAdes-3.11.1
PREFIX=/usr/local ./spades_compile.sh
