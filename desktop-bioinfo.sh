 
cd ~
echo "Aktualizacja systemu"
sudo apt-get update
sudo apt-get upgrade

mkdir ~/DOWNLOADS
DOWNLOADS=~/DOWNLOADS

echo "Kilka uzytecznych programów"
sudo apt install apt-transport-https build-essential ca-certificates catdoc cmake curl dirmngr fasttree gcc gdebi gedit gettext gfortran git gnupg2 inkscape iqtree jed kate libarchive-dev libblas-dev libbz2-dev libcurl4-openssl-dev libgtkmm-3.0-dev liblapack-dev liblzma-dev libssl-dev mafft make mrbayes muscle ncbi-blast+ ncbi-entrez-direct ncurses-* phyml pluma prank probcons python3-pip screenfetch software-properties-common t-coffee tree vim vim-gtk3 wget zlib1g-dev

# Samtools
echo "Instalacja samtools"
sudo apt-get install samtools

cd ~/DOWNLOADS

# Dendroscope
echo "Dendroscope"
wget https://software-ab.informatik.uni-tuebingen.de/download/dendroscope/Dendroscope_unix_3_7_6.sh
chmod u+x Dendroscope_unix_3_7_6.sh
./Dendroscope_unix_3_7_6.sh


# AliView
echo "AliView"
wget http://ormbunkar.se/aliview/downloads/linux/linux-version-1.28/aliview.install.run
chmod +x aliview.install.run
sudo ./aliview.install.run

# FigTree
echo "FigTree"
wget 'http://tree.bio.ed.ac.uk/download.php?id=96&num=3' -O FT.tgz
tar -xvzf FT.tgz
ft=`ls -d FigTree*`
cp $ft/bin/figtree $ft
chmod u+x $ft/figtree
cp -r $ft ~


# JmodelTest 2
echo "JmodelTest 2"
cd ~
wget https://github.com/ddarriba/jmodeltest2/files/157117/jmodeltest-2.1.10.tar.gz
tar -xvzf jmodeltest-2.1.10.tar.gz
# zmiana nazwy katalogu na bardziej przyjazny i uniwersalny
mv jmodeltest-2.1.10 jmodeltest
# pobranie manuala do katalogu programu
cd jmodeltest
wget https://github.com/ddarriba/jmodeltest2/files/157130/manual.pdf

# Powerline fonts
echo "Powerline fonts"
cd ~
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts

# Miniconda
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
# Uruchamianie: ~/miniconda3/bin/conda init bash


# Newick utils w miniconda
conda install bioconda::newick_utils

# Newick utils
echo "Newick utils"
#bash
cd ~/DOWNLOADS
#sudo apt install sudo apt install libluajit-5*
wget http://cegg.unige.ch/pub/newick-utils-1.6-Linux-x86_64-disabled-extra.tar.gz
tar -xvzf newick*
cd newick-utils-1.6
sudo cp src/nw_* /usr/local/bin

# r8s
echo "r8s"
sudo apt-get install make gcc gfortran
cd ~
wget https://sourceforge.net/projects/r8s/files/r8s1.81.tar.gz
tar -xvzf r8s1.81.tar.gz
mv r8s1.81 r8s
cd r8s/src/
sed -i 's/continuousML.o:continuousML.h/#continuousML.o:continuousML.h/' Makefile.linux
sed -i 's/blas.o tn.o TNwrapper.o continuousML.o ancestral.o covarion.o/blas.o tn.o TNwrapper.o ancestral.o covarion.o #continuousML.o/' Makefile.linux
sed -i 's|CFLAGS|LPATH = -L/usr/local/gfortran/lib # correct location as of August 2011.\nCFLAGS|' Makefile.linux
sed -i 's|memory.o: /usr/include/errno.h /usr/include/sys/errno.h|memory.o: /usr/include/errno.h # /usr/include/sys/errno.h|' Makefile.linux
sed -i 's|memory.o: /usr/include/stdlib.h memory.h|memory.o: /usr/include/stdlib.h # memory.h|' Makefile.linux
sed -i 's|CFLAGS = -g -std=c99 -pedantic|#CFLAGS = -g -std=c99 -pedantic |' Makefile.linux
make -f Makefile.linux
chmod u+x r8s
cp r8s ..
cd ..


# FAST - set of usefull tools for working with sequences
# see: https://github.com/tlawrence3/FAST

sudo perl -MCPAN -e 'install FAST'

# Je sli mrBayes nie działa prawidłowo

#Usuwanie mrBayesa (jeśli trzeba)
sudo apt remove mrbayes

# Instalacja Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# MOdyfikacja ustawień aby brew był widoczny
# zmień student na nazwę wlaściwego uztkownika
username=student
(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /home/$username/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# Instalacja mrBayes

brew tap brewsci/bio
brew install mrbayes

# latex, xelatex etc.
sudo apt install texlive-latex-extra-doc texlive-lang-polish texlive-fonts-recommended texlive-xetex texlive-lang-polish latexmk texlive-fonts-extra texlive-science texlive-extra-utils

 # fira font for metropolis theme
wget https://github.com/mozilla/Fira/archive/master.zip
unzip master.zip
sudo mkdir -p /usr/share/fonts/truetype/fira
sudo cp Fira-master/ttf/* /usr/share/fonts/truetype/fira

 # metropolis theme for beamer
 # https://github.com/matze/mtheme

git clone https://github.com/matze/mtheme.git
cd mtheme
make sty
sudo make install
