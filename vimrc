syntax on 

" set t_Co=256


set background=dark
set nu
set backspace=2
set ruler
set showcmd
set laststatus=2
":setlocal spell spelllang=pl,en 
set incsearch

colorscheme desert

set autochdir
