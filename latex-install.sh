# latex, xelatex etc.
sudo apt install texlive-latex-extra-doc texlive-lang-polish texlive-fonts-recommended texlive-xetex texlive-lang-polish latexmk texlive-fonts-extra texlive-science texlive-extra-utils
 
 # fira font for metropolis theme
wget https://github.com/mozilla/Fira/archive/master.zip 
unzip master.zip 
sudo mkdir -p /usr/share/fonts/truetype/fira 
sudo cp Fira-master/ttf/* /usr/share/fonts/truetype/fira

master=Fira_Code_v6.2
mkdir fira
cd fira
wget https://github.com/tonsky/FiraCode/releases/download/6.2/${master}.zip
unzip ${master}.zip
#sudo mkdir -p /usr/share/fonts/truetype/fira 
sudo cp ttf/* /usr/share/fonts/truetype/fira
sudo cp variable_ttf/* /usr/share/fonts/truetype/fira
cd ..
rm -rf fira
 
 # metropolis theme for beamer
 # https://github.com/matze/mtheme
 
git clone https://github.com/matze/mtheme.git
cd mtheme  
make sty
sudo make install 
 
 