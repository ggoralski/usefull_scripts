
cd ~
echo "Aktualizacja systemu"
sudo apt-get update
sudo apt-get upgrade

DOWNLOADS=~/DOWNLOADS
mkdir $DOWNLOADS


echo "Kilka uzytecznych programów"
sudo apt install apt-transport-https build-essential ca-certificates catdoc cmake curl dirmngr fasttree gcc gdebi gedit gettext gfortran git gnupg2 inkscape iqtree jed kate libarchive-dev libblas-dev libbz2-dev libcurl4-openssl-dev libgtkmm-3.0-dev liblapack-dev liblzma-dev libssl-dev mafft make muscle ncbi-blast+ ncbi-entrez-direct ncurses-* phyml pluma python3-pip screenfetch software-properties-common tree vim vim-gtk3 wget zlib1g-dev neofetch

# Samtools
echo "Instalacja samtools"
sudo apt install samtools

cd $DOWNLOADS


# Java 
sudo apt install default-jre 

# Dendroscope
echo "Dendroscope"
wget https://software-ab.cs.uni-tuebingen.de/download/dendroscope/Dendroscope_unix_3_8_10.sh
chmod u+x Dendroscope_unix_3_8_10.sh
./Dendroscope_unix_3_8_10.sh

# AliView
echo "AliView"
wget http://ormbunkar.se/aliview/downloads/linux/linux-version-1.28/aliview.install.run
chmod +x aliview.install.run
sudo ./aliview.install.run

# FigTree
echo "FigTree"
wget 'https://github.com/rambaut/figtree/releases/download/v1.4.4/FigTree_v1.4.4.tgz' -O FT.tgz
tar -xvzf FT.tgz
ft=`ls -d FigTree*`
cp $ft/bin/figtree $ft
chmod u+x $ft/figtree
cp -r $ft ~

# JmodelTest 2
echo "JmodelTest 2"
cd ~
wget https://github.com/ddarriba/jmodeltest2/files/157117/jmodeltest-2.1.10.tar.gz
tar -xvzf jmodeltest-2.1.10.tar.gz
# zmiana nazwy katalogu na bardziej przyjazny i uniwersalny
mv jmodeltest-2.1.10 jmodeltest
# pobranie manuala do katalogu programu
cd jmodeltest
wget https://github.com/ddarriba/jmodeltest2/files/157130/manual.pdf

# Powerline fonts
echo "Powerline fonts"
cd ~
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts

# Miniconda
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
# Uruchamianie: ~/miniconda3/bin/conda init bash

# Dodawanie kanałów:
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge

# Newick utils w miniconda
conda install bioconda::newick_utils


#!/bin/bash

# Skrypt pobierający i kompilujący program r8s

# Instalacja potrzebnych pakietów do kompilacji
sudo apt-get install make gcc gfortran
cd ~

# r8s https://github.com/iTaxoTools/pyr8s
git clone https://github.com/iTaxoTools/pyr8s.git
cd pyr8s/
pip install .

# Instalacja Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Modyfikacja ustawień aby brew był widoczny
# zmień student na nazwę wlaściwego uztkownika
username=student
(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /home/$username/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# Instalacja mrBayes

brew tap brewsci/bio
brew install mrbayes



